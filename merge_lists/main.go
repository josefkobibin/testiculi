package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	list1 := &ListNode{
		Val: 1,
		Next: &ListNode{
			Val: 2,
			Next: &ListNode{
				Val: 4,
				Next: &ListNode{
					Val:  2,
					Next: nil,
				},
			},
		},
	}
	list2 := &ListNode{
		Val: 1,
		Next: &ListNode{
			Val: 3,
			Next: &ListNode{
				Val: 4,
				Next: &ListNode{
					Val: 123,
					Next: &ListNode{
						Val: 11,
						Next: &ListNode{
							Val:  -90,
							Next: nil,
						},
					},
				},
			},
		},
	}
	mergeTwoLists(list1, list2)
}

func mergeTwoLists(list1 *ListNode, list2 *ListNode) *ListNode {
	if list1 == nil {
		if list2 == nil {
			return nil
		}
		return list2
	} else if list2 == nil {
		return list1
	}

	NewHead := new(ListNode)
	start := NewHead
	for list1 != nil || list2 != nil {
		if list1 != nil && list2 != nil && list1.Val < list2.Val {
			NewHead.Next = &ListNode{Val: list1.Val}
			NewHead = NewHead.Next
			NewHead.Next = &ListNode{Val: list2.Val}
			NewHead = NewHead.Next
			list1 = list1.Next
			list2 = list2.Next
		} else if list1 != nil && list2 != nil {
			NewHead.Next = &ListNode{Val: list2.Val}
			NewHead = NewHead.Next
			NewHead.Next = &ListNode{Val: list1.Val}
			NewHead = NewHead.Next
			list1 = list1.Next
			list2 = list2.Next
		} else if list2 == nil && list1 != nil {
			NewHead.Next = &ListNode{Val: list1.Val}
			NewHead = NewHead.Next
			list1 = list1.Next
		} else if list1 == nil && list2 != nil {
			NewHead.Next = &ListNode{Val: list2.Val}
			NewHead = NewHead.Next
			list2 = list2.Next
		}
	}

	sortList(start.Next)

	printList(start.Next)

	return NewHead
}

func sortList(node *ListNode) {
	counter := 0
	for node != nil {
		node = node.Next
		counter++
	}

}

func printList(node *ListNode) {
	for node != nil {
		fmt.Println(node.Val)
		node = node.Next
	}
}
