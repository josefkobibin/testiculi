package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println(topKFrequent([]int{1, 1, 1, 2, 2, 3, 4, 4, 4, 4, 4, 4, 5, 5, 5, 6, 6, 6, 6}, 4))
}

type ValFreq struct {
	freq int
	val  int
}

type ValFreqArr []ValFreq

func (a ValFreqArr) Len() int           { return len(a) }
func (a ValFreqArr) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ValFreqArr) Less(i, j int) bool { return a[i].freq < a[j].freq }

func topKFrequent(nums []int, k int) []int {
	m := make(map[int]int)

	for _, n := range nums {
		m[n]++
	}

	freqArr := make(ValFreqArr, 0, len(m))

	for k := range m {
		freqArr = append(freqArr, ValFreq{freq: m[k], val: k})
	}

	sort.Slice(freqArr, func(i, j int) bool {
		return freqArr[i].freq > freqArr[j].freq
	})

	result := make([]int, 0, len(freqArr[:k]))

	for i := range freqArr {
		fmt.Printf("n:%d, f:%d\n", freqArr[i].val, freqArr[i].freq)
	}

	for i := 0; i < k; i++ {
		result = append(result, freqArr[i].val)
	}

	return result
}
