package main

import "fmt"

func main() {
	fmt.Println(groupAnagrams([]string{"eat", "tea", "tan", "ate", "nat", "bat"}))
}

func groupAnagrams(strs []string) [][]string {
	output := make([][]string, 0)

	for i1, v1 := range strs {

		if isIn(output, v1) {
			continue
		}

		tmp := make([]string, 0)
		tmp = append(tmp, v1)

		for i2, v2 := range strs {
			if i1 == i2 {
				continue
			}

			if isAnagram(v1, v2) {
				tmp = append(tmp, v2)
			}
		}

		output = append(output, tmp)
	}

	return output
}

func isAnagram(s string, t string) bool {
	letters := make(map[rune]int)

	for _, letter := range s {
		letters[letter]++
	}

	for _, letter := range t {
		letters[letter]--
	}

	for letter := range letters {
		if letters[letter] != 0 {
			return false
		}
	}

	return true
}

func compareSlices(slice1, slice2 []string) bool {
	if len(slice1) != len(slice2) {
		return false
	}

	m := make(map[string]int, len(slice1))

	for _, v := range slice1 {
		m[v]++
	}

	for _, v := range slice2 {
		m[v]--
	}

	for k := range m {
		if m[k] != 0 {
			return false
		}
	}

	return true
}

func isIn(strs [][]string, elem string) bool {
	for i := 0; i < len(strs); i++ {
		for _, v := range strs[i] {
			if elem == v {
				return true
			}
		}
	}
	return false
}

/*

package main

import (
    "fmt"
    "sort"
)

func groupAnagrams(strs []string) [][]string {
    keyToAnagrams := make(map[string][]string)

    for _, str := range strs {
        chars := []byte(str)
        sort.Slice(chars, func(i, j int) bool {
            return chars[i] < chars[j]
        })
        key := string(chars)
        keyToAnagrams[key] = append(keyToAnagrams[key], str)
    }

    var result [][]string
    for _, anagrams := range keyToAnagrams {
        result = append(result, anagrams)
    }

    return result
}

*/
