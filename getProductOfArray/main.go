package main

import "fmt"

func main() {
	fmt.Println(productExceptSelf([]int{1, 0, 2, 3, -1, 4, 45, 4, 4, 3, 3, 4, -1, 1, 3, -1, 4, 3, -2, 2}))
}

func productExceptSelf(nums []int) []int {
	result := make([]int, 0, len(nums))

	for i := range nums {
		tmp := make([]int, len(nums))
		copy(tmp, nums)

		tmp = append(tmp[:i], tmp[i+1:]...)

		if containsZero(tmp) {
			result = append(result, 0)
		} else {
			result = append(result, getProduct(tmp))
		}

	}

	return result
}

func getProduct(nums []int) int {
	result := 1

	sign := 0
	for _, v := range nums {
		if v == 1 {
			continue
		} else if v == -1 {
			sign++
		} else {
			result *= v
		}
	}

	if sign%2 == 0 {
		return result
	} else {
		return -result
	}
}

func containsZero(nums []int) bool {
	for _, v := range nums {
		if v == 0 {
			return true
		}
	}

	return false
}
