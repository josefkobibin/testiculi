package main

import (
	"fmt"
)

var sample = []rune("adfasaaaaaaaaaaaaaasdddddddd")

func main() {
	if len(sample) == 1 {
		fmt.Println(string(sample))
		return
	} else if len(sample) == 0 {
		return
	}

	substrings := [][]rune{}

	for i := 0; i < len(sample)-1; i++ {
		sub := []rune{}
		if sample[i] != sample[i+1] {
			sub = append(sub, sample[i])
		} else {
			x := i
			for x < len(sample) && sample[x] == sample[i] {
				sub = append(sub, sample[x])
				x++
			}
			i += len(sub) - 1
		}
		substrings = append(substrings, sub)
	}

	maxsub := substrings[0]
	for _, sub := range substrings {
		if len(sub) > len(maxsub) {
			maxsub = sub
		}
	}

	fmt.Println(string(maxsub))
}
