package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	a := new(ListNode)
	b := new(ListNode)
	c := new(ListNode)
	d := new(ListNode)
	e := new(ListNode)

	a.Next = b
	b.Next = c
	c.Next = d
	d.Next = e
	e.Next = nil

	fmt.Println(findLoop(a))
}

func findLoop(head *ListNode) bool {
	loop := make(map[*ListNode]struct{})

	for head != nil {
		if _, ok := loop[head]; ok {
			return true
		} else {
			loop[head] = struct{}{}
		}
		head = head.Next
	}

	return false
}
