package main

import (
	"fmt"
	"slices"
)

func main() {
	fmt.Println(isAnagram("asdf", "fasd"))
}

func isAnagram(s string, t string) bool {
	if len(s) != len(t) {
		return false
	}

	sRune := []rune(s)
	tRune := []rune(t)

	slices.Sort(sRune)
	slices.Sort(tRune)

	for i := 0; i < len(s); i++ {
		if sRune[i] != tRune[i] {
			return false
		}
	}

	return true
}
