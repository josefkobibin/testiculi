package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(isPalindrome("A man, a plan, a canal: Panama"))
}

const alph = "abcdefghijklmnopqrstuvxyz1234567890"

func isPalindrome(s string) bool {
	newS := strings.ToLower(s)

	var rawString string
	for _, v := range newS {
		if isAlphanumeric(v) {
			rawString += string(v)
		}
	}

	var reversedRawString string
	for i := len(rawString) - 1; i > -1; i-- {
		reversedRawString += string(rawString[i])
	}

	fmt.Println(rawString, reversedRawString)

	return rawString == reversedRawString
}

func isAlphanumeric(s rune) bool {
	for _, r := range alph {
		if r == s {
			return true
		}
	}

	return false
}
