package main

import "fmt"

func main() {
	fmt.Println(isAnagram("asdf", "fasd"))
}

func isAnagram(s string, t string) bool {
	letters := make(map[rune]int)

	for _, letter := range s {
		letters[letter]++
	}

	for _, letter := range t {
		letters[letter]--
	}

	for letter := range letters {
		if letters[letter] != 0 {
			return false
		}
	}

	return true
}
