package main

import "fmt"

type Node struct {
	data int
	next *Node
}

func reverseLinkedList(node *Node) *Node {
	if node == nil || node.next == nil {
		return node
	}

	newHead := reverseLinkedList(node.next)
	node.next.next = node
	node.next = nil

	return newHead
}

func printLinkedList(node *Node) {
	for node != nil {
		fmt.Print(node.data, " ")
		node = node.next
	}
	fmt.Println()
}

func main() {
	head := &Node{data: 1}
	head.next = &Node{data: 2}
	head.next.next = &Node{data: 3}
	head.next.next.next = &Node{data: 4}
	head.next.next.next.next = &Node{data: 5}

	fmt.Println("Original Linked List:")
	printLinkedList(head)

	// Reversing the linked list
	head = reverseLinkedList(head)

	fmt.Println("Reversed Linked List:")
	printLinkedList(head)
}
