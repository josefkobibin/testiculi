package main

import "fmt"

func main() {
	a := []int{1, 3, 4, 5, 3, 2, 4}
	fmt.Println(containsDuplicate(a))
}

func containsDuplicate(nums []int) bool {
	m := make(map[int]struct{}, len(nums))

	for _, v := range nums {
		if _, ok := m[v]; ok {
			return true
		} else {
			m[v] = struct{}{}
		}
	}

	return false
}
