package main

import "fmt"

func main() {
	fmt.Println(twoSum([]int{1, 2, 3, 4, 5, 6}, 10))
}

func twoSum(nums []int, target int) []int {
	result := make([]int, 0, 2)

	tmp := target
	for i1, v1 := range nums {
		target = tmp
		target -= v1
		for i2, v2 := range nums {
			if i1 == i2 {
				continue
			}
			if target == v2 {
				result = append(result, i1, i2)
				return result
			}
		}
	}

	return nil
}
